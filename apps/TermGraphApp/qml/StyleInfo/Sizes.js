/*
 *  TermGraph, build graph of knowledge.
 *  Copyright © 2016-2020. Savenkov Igor. All rights reserved
 *  Contacts: dev.savenkovigor@protonmail.com
 *
 *  This file is part of TermGraph.
 *
 *  TermGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TermGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with TermGraph. If not, see <https://www.gnu.org/licenses/>.
 */

.import Api 1.0 as Api

var baseX100 = base * 2;
var baseX75  = base * 1.75;
var baseX50  = base * 1.5;
var baseX25  = base * 1.25;
var base     = Api.Application.getUiElementSize("roundButton");
var baseR25  = base * 0.75;
var baseR50  = base * 0.5;
var baseR75  = base * 0.25;

var button = Api.Application.getUiElementSize("button");
var colSpace = Api.Application.getUiElementSize("colSpace");
