/*
 *  TermGraph, build graph of knowledge.
 *  Copyright © 2016-2020. Savenkov Igor. All rights reserved
 *  Contacts: dev.savenkovigor@protonmail.com
 *
 *  This file is part of TermGraph.
 *
 *  TermGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TermGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with TermGraph. If not, see <https://www.gnu.org/licenses/>.
 */

.pragma library

const book            = "qrc:/book.svg"
const check           = "qrc:/check.svg"
const chevronLeft     = "qrc:/chevron-left.svg"
const chevronRight    = "qrc:/chevron-right.svg"
const code            = "qrc:/code.svg"
const cog             = "qrc:/cog.svg"
const crossOnCircle   = "qrc:/crossOnCircle.svg"
const eye             = "qrc:/eye.svg"
const folder          = "qrc:/folder.svg"
const info            = "qrc:/info.svg"
const leftArrow       = "qrc:/arrow-thick-left.svg"
const list            = "qrc:/list.svg"
const loopCircular    = "qrc:/loop-circular.svg"
const magnifyingGlass = "qrc:/magnifying-glass.svg"
const menu            = "qrc:/menu.svg"
const options         = "qrc:/options.svg"
const pencil          = "qrc:/pencil.svg"
const plus            = "qrc:/plus.svg"
const questionMark    = "qrc:/question-mark.svg"
const rightArrow      = "qrc:/arrow-thick-right.svg"
const share           = "qrc:/share.svg"
const spreadsheet     = "qrc:/spreadsheet.svg"
const trash           = "qrc:/trash.svg"
const warning         = "qrc:/warning.svg"
