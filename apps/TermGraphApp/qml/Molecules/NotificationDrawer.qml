/*
 *  TermGraph, build graph of knowledge.
 *  Copyright © 2016-2020. Savenkov Igor. All rights reserved
 *  Contacts: dev.savenkovigor@protonmail.com
 *
 *  This file is part of TermGraph.
 *
 *  TermGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TermGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with TermGraph. If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick
import QtQuick.Controls

import Api

import StyleInfo

Drawer {
    id: root

    implicitHeight: topPadding + implicitContentHeight + bottomPadding
    interactive: position !== 0.0
    edge: Qt.BottomEdge
    dim: false

    onOpened: Notification.handleNotifyShow();
    onClosed: Notification.handleNotifyHide();

    Component.onCompleted: {
        Qt.callLater(function() {
            Notification.handleUiInitialization();
        });
    }

    function setTextAndOpen(text) {
        infoLabel.text = text;
        root.open();
    }

    function showDebug(debugInfo) { setTextAndOpen("Debug: " + debugInfo);        }
    function showError(error)     { setTextAndOpen("Ошибка: " + error);           }
    function showWarning(warning) { setTextAndOpen("Предупреждение: " + warning); }
    function showInfo(info)       { setTextAndOpen("Информация: " + info);        }

    Connections {
        target: Notification

        function onShowInfoQml(info)       { root.showInfo(info) }
        function onShowWarningQml(warning) { root.showWarning(warning) }
        function onShowErrorQml(error)     { root.showError(error) }
        function onShowDebugQml(debugInfo) { root.showDebug(debugInfo) }
        function onHideNotify()            { root.close() }
    }

    contentItem: TextArea {
        id: infoLabel

        text: ""
        readOnly: true
        wrapMode: TextEdit.Wrap

        horizontalAlignment: Text.AlignHCenter
        font: Fonts.inputText

        leftPadding: Sizes.baseR50
        rightPadding: Sizes.baseR50
    }
}
