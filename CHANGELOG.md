# Changelog
Notable changes would be mentioned in this file

## [2.0.0] - 2020-06-06

### Added
- Auto format tool cmake-format for cmake files cleaning.
- Debug coloring technique
- Smooth navigation on scene
- Proper scaling for big scenes! For Desktop & Android

### Changed
- Replace scene engine with new one. Dramatically decrease memory usage.
- Big inner refactor in scene
- Move to Qt 5.15
- 25x time speed up for selection & change view of selection
- Help screen refactor, startup sequence reworked
- Move to cmake build tools
- Inner cpp part is now much more clear and 'constant'
- Move test cases to cmake
- Add multithreading for group building and clarify memory work
- Add animation for loading process
- Switch to cpp20

### Fixed
- Click out of group borders now works properly

## [1.0.0] - 2020-01-19
### First android version, published on Play Market
